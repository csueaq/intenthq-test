package com.intentHQ.battleship.ship.impl;

import com.intentHQ.battleship.ship.Direction;
import com.intentHQ.battleship.ship.Ship;
import com.intentHQ.battleship.ship.Turn;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShipImplTest {

    ShipImpl testShip;

    @Before
    public void setUp() throws Exception {
        testShip = new ShipImpl(3,3, Direction.North);
    }

    @Test
    public void testMove() throws Exception {
        //given
        int expectedX = 3;
        int expectedY = 5;

        //when
        testShip.move();
        testShip.move();

        //then
        assertEquals(expectedX,testShip.getCoordinateX());
        assertEquals(expectedY,testShip.getCoordinateY());

    }

    @Test
    public void testTurn() throws Exception {
        //given
        Direction expectedFacing = Direction.East;

        //when
        testShip.turn(Turn.Left);
        testShip.turn(Turn.Right);
        testShip.turn(Turn.Right);

        //then
        assertEquals(expectedFacing,testShip.getFacing());
    }

    @Test
    public void testMoveAndTurn() throws Exception {
        //given
        int expectedX = 2;
        int expectedY = 4;

        //when
        testShip.turn(Turn.Left);
        testShip.move();
        testShip.turn(Turn.Right);
        testShip.move();

        //then
        assertEquals(expectedX,testShip.getCoordinateX());
        assertEquals(expectedY,testShip.getCoordinateY());
    }

    @Test
    public void testMoveOutOfXBoundaryCoordinateXShouldNotChange() throws Exception {
        //given
        int expectedX = 5;
        testShip.setCoordinateXBoundary(5);

        //when
        testShip.turn(Turn.Right);
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();

        //then
        assertEquals(expectedX,testShip.getCoordinateX());
    }

    @Test
    public void testMoveOutOfZeroBoundaryOnYCoordinateYShouldNotChange() throws Exception {
        //given
        int expectedY = 0;

        //when
        testShip.turn(Turn.Right);
        testShip.turn(Turn.Right);
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();

        //then
        assertEquals(expectedY,testShip.getCoordinateY());
    }

    @Test
    public void testMoveOutOfZeroBoundaryOnXCoordinateXShouldNotChange() throws Exception {
        //given
        int expectedX = 0;

        //when
        testShip.turn(Turn.Left);
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();

        //then
        assertEquals(expectedX,testShip.getCoordinateX());
    }

    @Test
    public void testMoveOutOfYBoundaryCoordinateYShouldNotChange() throws Exception {
        //given
        int expectedY = 5;
        testShip.setCoordinateYBoundary(5);

        //when
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();
        testShip.move();

        //then
        assertEquals(expectedY,testShip.getCoordinateY());
    }
}

