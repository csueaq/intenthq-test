package com.intentHQ.battleship;

import com.intentHQ.battleship.battleShipGame.BattleShipGameImpl;
import com.intentHQ.battleship.board.Board;
import com.intentHQ.battleship.ship.Direction;
import com.intentHQ.battleship.ship.impl.ShipImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created with IntelliJ IDEA.
 * User: xinjin
 * Date: 30/12/13
 * Time: 19:47
 * To change this template use File | Settings | File Templates.
 */
public class BattleShipCommandLineVersionTest {

    BattleShipCommandLineVersion battleShipCommandLineVersion;

    @Mock
    Board board;
    @Mock
    BattleShipGameImpl battleShipGame;


    @Before
    public void setUp() throws Exception {
        initMocks(this);
        battleShipCommandLineVersion = new BattleShipCommandLineVersion();
        battleShipCommandLineVersion.setBattleShipGame(battleShipGame);
    }

    @Test
    public void testProcessInputInitBoard() throws Exception {
        //given
        String testInput = "4,5";
        int expectedX = 4;
        int expectedY = 5;
        when(battleShipGame.getBoard()).thenReturn(null);

        //when
        battleShipCommandLineVersion.processInput(testInput);

        //then
        verify(battleShipGame).setBoard(new Board(expectedX,expectedY));
        verify(battleShipGame).initBoard();

    }

    @Test
    public void testProcessInputAddShip() throws Exception {
        //given
        String testInput = "4,5,N";
        int expectedX = 4;
        int expectedY = 5;
        Direction expectedFacing = Direction.North;
        when(battleShipGame.getBoard()).thenReturn(board);
        when(battleShipGame.getShips()).thenReturn(new ArrayList<ShipImpl>());
        //when
        battleShipCommandLineVersion.processInput(testInput);

        //then
        verify(battleShipGame).addShip(new ShipImpl(expectedX,expectedY,expectedFacing));
    }

    @Test
    public void testProcessInputMoveShip() throws Exception {
        //given
        String testInput = "1,2 M";

        ShipImpl testShip = new ShipImpl(1,2,Direction.North);
        List<ShipImpl> testShips = new ArrayList<ShipImpl>();
        testShips.add(testShip);
        when(battleShipGame.getBoard()).thenReturn(board);
        when(battleShipGame.getShips()).thenReturn(testShips);
        when(battleShipGame.getShip(1,2)).thenReturn(testShip);

        //when
        battleShipCommandLineVersion.processInput(testInput);

        //then
        verify(battleShipGame).moveShip(testShip);
    }

    @Test
    public void testProcessInputFire() throws Exception {
        //given
        String testInput = "1,2";
        int expectedX = 1;
        int expectedY = 2;
        ShipImpl testShip = new ShipImpl(1,2,Direction.North);
        List<ShipImpl> testShips = new ArrayList<ShipImpl>();
        testShips.add(testShip);
        when(battleShipGame.getBoard()).thenReturn(board);
        when(battleShipGame.getShips()).thenReturn(testShips);

        //when
        battleShipCommandLineVersion.processInput(testInput);

        //then
        verify(battleShipGame).fire(expectedX,expectedY);
    }


}
