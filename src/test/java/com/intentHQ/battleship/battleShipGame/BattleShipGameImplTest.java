package com.intentHQ.battleship.battleShipGame;

import com.intentHQ.battleship.board.Board;
import com.intentHQ.battleship.board.SquareStatus;
import com.intentHQ.battleship.exceptions.OutOfBoardBoundaryException;
import com.intentHQ.battleship.exceptions.SquareOccupiedException;
import com.intentHQ.battleship.ship.Direction;
import com.intentHQ.battleship.ship.Turn;
import com.intentHQ.battleship.ship.impl.ShipImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created with IntelliJ IDEA.
 * User: xinjin
 * Date: 30/12/13
 * Time: 01:00
 * To change this template use File | Settings | File Templates.
 */
public class BattleShipGameImplTest {

    BattleShipGameImpl battleShipGame;

    @Mock
    ShipImpl ship1;
    @Mock
    ShipImpl ship2;
    @Mock
    Board board;


    @Before
    public void setUp() throws Exception {
        initMocks(this);
        battleShipGame = new BattleShipGameImpl();
        battleShipGame.setBoard(board);
        battleShipGame.initBoard();
    }

    @Test
    public void testFire() throws Exception {
        //given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        battleShipGame.addShip(ship1);
        when(ship2.getCoordinateX()).thenReturn(2);
        when(ship2.getCoordinateY()).thenReturn(2);
        battleShipGame.addShip(ship2);
        when(board.getSquareStatus(ship2.getCoordinateX(), ship2.getCoordinateY())).thenReturn(SquareStatus.Occupied);

        //when
        battleShipGame.fire(ship2.getCoordinateX(),ship2.getCoordinateY());

        //then
        verify(ship2).setSunk(true);
        verify(board).setShipSunkAtCoordinate(ship2.getCoordinateX(), ship2.getCoordinateY());

    }


    @Test
    public void testAddShip() throws Exception {
        //given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        battleShipGame.addShip(ship1);
        when(ship2.getCoordinateX()).thenReturn(2);
        when(ship2.getCoordinateY()).thenReturn(2);

        //when
        battleShipGame.addShip(ship2);

        //then
        verify(ship2).setCoordinateXBoundary(board.getCoordinateXBoundary());
        verify(ship2).setCoordinateYBoundary(board.getCoordinateYBoundary());
        verify(board).setOccupiedAtCoordinate(ship2.getCoordinateX(),ship2.getCoordinateY());

    }

    @Test(expected=SquareOccupiedException.class)
    public void testAddShipThrowSquareOccupiedException() throws Exception{

        // given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        when(ship1.getCoordinateX()).thenReturn(2);
        when(ship1.getCoordinateY()).thenReturn(2);
        battleShipGame.addShip(ship1);
        when(ship2.getCoordinateX()).thenReturn(2);
        when(ship2.getCoordinateY()).thenReturn(2);

        //when
        battleShipGame.addShip(ship2);

        //then throw SquareOccupiedException
    }

    @Test(expected=OutOfBoardBoundaryException.class)
    public void testAddShipThrowOutOfBoardBoundaryException() throws Exception{

        // given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        when(ship1.getCoordinateX()).thenReturn(10);
        when(ship1.getCoordinateY()).thenReturn(10);

        //when
        battleShipGame.addShip(ship1);

        //then throw OutOfBoardBoundaryException
    }


    @Test
    public void testMoveShip() throws Exception {
        //given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        when(ship1.getCoordinateX()).thenReturn(2);
        when(ship1.getCoordinateY()).thenReturn(2);
        when(ship1.getFacing()).thenReturn(Direction.North);
        battleShipGame.addShip(ship1);
        int prevX = ship1.getCoordinateX();
        int prevY = ship1.getCoordinateY();


        //when
        battleShipGame.moveShip(ship1);

        //then
        verify(board).setEmptyAtCoordinate(prevX,prevY);
        verify(ship1).move();


    }

    @Test(expected=OutOfBoardBoundaryException.class)
    public void testMoveThrowOutOfBoardBoundaryException() throws Exception{
        //given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        when(ship1.getCoordinateX()).thenReturn(2);
        when(ship1.getCoordinateY()).thenReturn(0);
        when(ship1.getNextMovingCoordinateX()).thenReturn(2);
        when(ship1.getNextMovingCoordinateY()).thenReturn(-1);
        when(ship1.getFacing()).thenReturn(Direction.North);
        battleShipGame.addShip(ship1);

        //when
        battleShipGame.moveShip(ship1);

        //then throw OutOfBoardBoundaryException

    }

    @Test(expected=SquareOccupiedException.class)
    public void testMoveThrowSquareOccupiedException() throws Exception{
        //given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        when(ship1.getCoordinateX()).thenReturn(2);
        when(ship1.getCoordinateY()).thenReturn(0);
        when(ship1.getNextMovingCoordinateX()).thenReturn(2);
        when(ship1.getNextMovingCoordinateY()).thenReturn(1);
        when(ship1.getFacing()).thenReturn(Direction.West);
        when(ship1.getCoordinateX()).thenReturn(2);
        when(ship1.getCoordinateY()).thenReturn(1);
        battleShipGame.addShip(ship1);
        battleShipGame.addShip(ship2);

        //when
        battleShipGame.moveShip(ship1);

        //then throw SquareOccupiedException

    }

    @Test
    public void testTurnShip() throws Exception {
        //given
        battleShipGame.addShip(ship1);

        //when
        battleShipGame.turnShip(ship1, Turn.Left);

        //then
        verify(ship1).turn(Turn.Left);
    }

    @Test
    public void testGetShip() throws Exception {
        //given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        when(ship1.getCoordinateX()).thenReturn(2);
        when(ship1.getCoordinateY()).thenReturn(0);
        battleShipGame.addShip(ship1);
        battleShipGame.addShip(ship2);

        //when
        ShipImpl actualShip = battleShipGame.getShip(ship1.getCoordinateX(),ship1.getCoordinateY());

        //then
        assertEquals(ship1,actualShip);
    }

    @Test
    public void testGetShipReturnNull() throws Exception {
        //given
        when(board.getCoordinateXBoundary()).thenReturn(4);
        when(board.getCoordinateYBoundary()).thenReturn(4);
        when(ship1.getCoordinateX()).thenReturn(2);
        when(ship1.getCoordinateY()).thenReturn(0);
        battleShipGame.addShip(ship1);
        battleShipGame.addShip(ship2);

        //when
        ShipImpl actualShip = battleShipGame.getShip(1,3);

        //then
        assertEquals(null,actualShip);
    }
}
