package com.intentHQ.battleship.board;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: xinjin
 * Date: 29/12/13
 * Time: 23:13
 * To change this template use File | Settings | File Templates.
 */
public class BoardTest {
    Board testBoard;

    @Before
    public void setUp() throws Exception {
        testBoard = new Board(5,5);
    }

    @Test
    public void testInit() throws Exception {
        //given a new board
        int expectedNumOfEmptySquare = 25;

        //when
        testBoard.init();
        int actualNumOfEmptySquare = 0;
        for(int x=0; x<=testBoard.getCoordinateXBoundary();x++){
            for(int y=0; y<=testBoard.getCoordinateYBoundary();y++) {
                if(testBoard.getSquareStatus(x,y)== SquareStatus.Empty)
                    actualNumOfEmptySquare++;
            }
        }

        //then
        assertEquals(expectedNumOfEmptySquare,actualNumOfEmptySquare);
    }
}
