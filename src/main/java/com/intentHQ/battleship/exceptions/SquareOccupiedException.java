package com.intentHQ.battleship.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: xinjin
 * Date: 30/12/13
 * Time: 00:47
 * To change this template use File | Settings | File Templates.
 */
public class SquareOccupiedException extends Exception {

    //Constructor that accepts a message
    public SquareOccupiedException(String message)
    {
        super(message);
    }
}
