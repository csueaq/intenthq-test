package com.intentHQ.battleship.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: xinjin
 * Date: 30/12/13
 * Time: 01:33
 * To change this template use File | Settings | File Templates.
 */
public class OutOfBoardBoundaryException extends Exception{

    //Constructor that accepts a message
    public OutOfBoardBoundaryException(String message)
    {
        super(message);
    }
}
