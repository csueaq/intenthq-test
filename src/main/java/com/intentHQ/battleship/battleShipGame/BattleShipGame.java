package com.intentHQ.battleship.battleShipGame;


import com.intentHQ.battleship.exceptions.OutOfBoardBoundaryException;
import com.intentHQ.battleship.exceptions.SquareOccupiedException;
import com.intentHQ.battleship.ship.Turn;
import com.intentHQ.battleship.ship.impl.ShipImpl;

public interface BattleShipGame {

   /**
    * fire from a ship to a coordinate (X,Y), if the coordinate firing at has a ship on the the this method should update the board to indicate the sink of that ship
    * @param coordinateX the coordinate X to fire at
    * @param coordinateY the coordinate Y to fire at
    * */
   void fire(int coordinateX,int coordinateY);

   /**
    * Initialise a board for the game
    * */
   void initBoard();

   /**
    * add a ship on tp the board, if there's a ship on the square about to add, no ship is added and throws SquareOccupiedException
    * @param ship the ship to add
    * @exception SquareOccupiedException if there's a ship on the square about to add
    * */
   void addShip(ShipImpl ship) throws SquareOccupiedException, OutOfBoardBoundaryException;


   /**
    * Move a given ship
    * @param ship the ship
    * @exception SquareOccupiedException if there's a ship on the square about to move to
    * */
   void moveShip(ShipImpl ship) throws SquareOccupiedException, OutOfBoardBoundaryException;

   /**
    * turn a given ship
    * @param ship the ship
    * @param turn the direction to turn
    * */
   void turnShip(ShipImpl ship,Turn turn);

   /**
    * get a ship on given coordinates
    * @param coordinateX the coordinate for X
    * @param coordinateY the coordinate for Y
    * @return the ShipImpl object whose coordinateX and coordinateY are the same as params, or null
    * */
   ShipImpl getShip(int coordinateX, int coordinateY);
}
