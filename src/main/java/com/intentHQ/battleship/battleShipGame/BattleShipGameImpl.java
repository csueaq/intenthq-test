package com.intentHQ.battleship.battleShipGame;

import com.intentHQ.battleship.exceptions.OutOfBoardBoundaryException;
import com.intentHQ.battleship.exceptions.SquareOccupiedException;
import com.intentHQ.battleship.board.Board;
import com.intentHQ.battleship.board.SquareStatus;
import com.intentHQ.battleship.ship.Turn;
import com.intentHQ.battleship.ship.impl.ShipImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: xinjin
 * Date: 29/12/13
 * Time: 23:36
 * To change this template use File | Settings | File Templates.
 */
public class BattleShipGameImpl implements BattleShipGame {


    private Board board;
    private List<ShipImpl> ships;

    public BattleShipGameImpl() {
        ships = new ArrayList<ShipImpl>();
    }
    @Override
    public void fire(int coordinateX, int coordinateY) {
        if(board.getSquareStatus(coordinateX,coordinateY) == SquareStatus.Occupied) {
            ShipImpl shipOnCoordinate = getShip(coordinateX,coordinateY);
            if(shipOnCoordinate!=null) {
                setShipSunk(shipOnCoordinate);
            }
        }
    }

    @Override
    public void initBoard() {
        board.init();
    }

    @Override
    public void addShip(ShipImpl ship) throws SquareOccupiedException, OutOfBoardBoundaryException {
        if(getShip(ship.getCoordinateX(),ship.getCoordinateY()) == null) {
            if(isOutOfBoundary(ship.getCoordinateX(),ship.getCoordinateY())) {
                throw new OutOfBoardBoundaryException("The coordinate for this ship is out of boundary of the board");
            } else {
                ship.setCoordinateXBoundary(board.getCoordinateXBoundary());
                ship.setCoordinateYBoundary(board.getCoordinateYBoundary());
                ships.add(ship);
                board.setOccupiedAtCoordinate(ship.getCoordinateX(),ship.getCoordinateY());
            }
        } else {
            throw new SquareOccupiedException("This square has been occupied");
        }

    }

    @Override
    public void moveShip(ShipImpl ship) throws SquareOccupiedException, OutOfBoardBoundaryException {
        if(getShip(ship.getNextMovingCoordinateX(),ship.getNextMovingCoordinateY())==null) {
            if(isOutOfBoundary(ship.getNextMovingCoordinateX(),ship.getNextMovingCoordinateY())) {
                throw new OutOfBoardBoundaryException("The coordinate for this move is out of boundary of the board");
            } else {
                board.setEmptyAtCoordinate(ship.getCoordinateX(),ship.getCoordinateY());
                ship.move();
                board.setOccupiedAtCoordinate(ship.getCoordinateX(),ship.getCoordinateY());
            }
        } else {
            throw new SquareOccupiedException("The square you want to move to has been occupied, please go around");
        }

    }

    @Override
    public void turnShip(ShipImpl ship,Turn turn) {
        ship.turn(turn);
    }

    @Override
    public ShipImpl getShip(int coordinateX, int coordinateY) {
        for(ShipImpl ship : ships) {
            if(ship.getCoordinateX()==coordinateX && ship.getCoordinateY()==coordinateY)
                return ship;
        }
        return null;
    }

    private void setShipSunk(ShipImpl ship) {
        int index = ships.indexOf(ship);
        ship.setSunk(true);
        ships.set(index,ship);
        board.setShipSunkAtCoordinate(ship.getCoordinateX(),ship.getCoordinateY());
    }
    private boolean isOutOfBoundary(int coordinateX,int coordinateY) {
        return coordinateX > board.getCoordinateXBoundary() || coordinateY > board.getCoordinateYBoundary() || coordinateX < 0 || coordinateY < 0;
    }
    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public List<ShipImpl> getShips() {
        return ships;
    }

    public void setShips(List<ShipImpl> ships) {
        this.ships = ships;
    }


}
