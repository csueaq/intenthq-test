package com.intentHQ.battleship;

import com.intentHQ.battleship.battleShipGame.BattleShipGameImpl;
import com.intentHQ.battleship.board.Board;
import com.intentHQ.battleship.exceptions.OutOfBoardBoundaryException;
import com.intentHQ.battleship.exceptions.SquareOccupiedException;
import com.intentHQ.battleship.ship.Direction;
import com.intentHQ.battleship.ship.Turn;
import com.intentHQ.battleship.ship.impl.ShipImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: xinjin
 * Date: 30/12/13
 * Time: 02:03
 * To change this template use File | Settings | File Templates.
 */
public class BattleShipCommandLineVersion {

    public static final String patternBoard = "^([0-9]+)(,)([0-9]+)$";
    public static final String patternShip = "^([0-9]+)(,)([0-9]+)(,)([N|E|S|W])$";
    public static final String patternFire = "^([0-9]+)(,)([0-9]+)$";
    public static final String patternMove = "^([0-9]+)(,)([0-9]+)( )([L|R|M]+)$";

    private BattleShipGameImpl battleShipGame;

    public BattleShipCommandLineVersion() {
        battleShipGame = new BattleShipGameImpl();
    }
    public static void main(String[] args) {

        BattleShipCommandLineVersion battleShipCommandLineVersion = new BattleShipCommandLineVersion();
        System.out.println("Welcome to the battle ship game (enter 'usage' to find out how to play), please initialise the board first: ");

        while(true) {
            try{
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                String s = bufferRead.readLine();

                if(s.equals("quit"))
                    break;

                battleShipCommandLineVersion.processInput(s);

            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }

    }

    public BattleShipGameImpl getBattleShipGame() {
        return battleShipGame;
    }

    public void setBattleShipGame(BattleShipGameImpl battleShipGame) {
        this.battleShipGame = battleShipGame;
    }

    public static void showUsage() {
        System.out.println("Usage : ");
        System.out.println("First, you will need to setup a board, just type in '5,5' will setup a 5 X 5 board for you.");
        System.out.println("Then, you can start adding ships, this program only allow adding ships once, " +
                "so please add the all the ships in one go. Just type in '5,5,N 4,4,E' and this will add two ships for you.");
        System.out.println("When all above is done, you can start playing, you have 2 options, one is select a ship and turn or move it, " +
                "type in '1,2 LM' will move the ship at (1,2) and turn it to left and move it.");
        System.out.println("The other option is to fire, just type in '3,4', and it will fire at (3,4), if there's a ship at that coordinates it will sink");
        System.out.println("After adding all the ships, type in 'show ships', this will show status of all the ships we have");
        System.out.println("Type in 'quit' to exit the program");

    }
    public void processInput(String input) {
        if(input.equals("usage"))
            showUsage();
        else {
            if(battleShipGame.getBoard()==null) {
                actionInitBoard(input);
            } else if(battleShipGame.getShips().size()==0) {
                String[] parts = input.split(" ");
                for(int i=0;i<parts.length;i++)
                    actionAddShips(parts[i]);
            } else {
                if(input.equals("show ships"))
                    actionShowShips();
                else
                    actionOther(input);
            }
        }
    }

    private void actionShowShips() {
        for (ShipImpl ship : battleShipGame.getShips()) {
            String sunk = "";
            if(ship.isSunk())
                sunk="SUNK";
            System.out.println("("+ship.getCoordinateX()+","+ship.getCoordinateY()+","+ship.getFacing().toString()+") "+sunk);
        }
    }
    private void actionInitBoard(String input) {
        Pattern r = Pattern.compile(BattleShipCommandLineVersion.patternBoard);
        Matcher m = r.matcher(input);
        if(m.find()){
            int numberOfSquareOnX = Integer.parseInt(m.group(1));
            int numberOfSquareOnY = Integer.parseInt(m.group(3));
            battleShipGame.setBoard(new Board(numberOfSquareOnX,numberOfSquareOnY));
            battleShipGame.initBoard();
            System.out.println("Board of "+ numberOfSquareOnX + " X " + numberOfSquareOnY + " has been setup");
        } else {
            System.out.println("Please enter something like '5,5' to initialize the board");
        }
    }

    private void actionAddShips(String input) {
        Pattern r = Pattern.compile(BattleShipCommandLineVersion.patternShip);
        Matcher m = r.matcher(input);
        if(m.find()){
            int startingX = Integer.parseInt(m.group(1));
            int startingY = Integer.parseInt(m.group(3));
            Direction facing = getDirection(m.group(5).toString().charAt(0));
            ShipImpl newShip = new ShipImpl(startingX,startingY,facing);
            try{
                battleShipGame.addShip(newShip);
                System.out.println("Ship starting at (" + newShip.getCoordinateX()+","+newShip.getCoordinateY()+") facing "+newShip.getFacing().toString()+" has been added");
            } catch (SquareOccupiedException e) {
                System.out.println(e.getMessage());
            } catch (OutOfBoardBoundaryException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("Please make sure each ship you want to add is in the format of something like '5,5,N 4,4,E'");
        }
    }

    private void actionOther(String input) {
        Pattern firePattern = Pattern.compile(BattleShipCommandLineVersion.patternFire);
        Matcher fireMatcher = firePattern.matcher(input);
        Pattern movePattern = Pattern.compile(BattleShipCommandLineVersion.patternMove);
        Matcher moveMatcher = movePattern.matcher(input);

        if(moveMatcher.find()){
            int coordinateX = Integer.parseInt(moveMatcher.group(1));
            int coordinateY = Integer.parseInt(moveMatcher.group(3));
            char[] actions = moveMatcher.group(5).toCharArray();
            actionMove(coordinateX,coordinateY,actions);
        } else if(fireMatcher.find()){
            int coordinateX = Integer.parseInt(fireMatcher.group(1));
            int coordinateY = Integer.parseInt(fireMatcher.group(3));
            actionFire(coordinateX,coordinateY);
        } else {
            System.out.println("Please enter something like '3,2 LMLMRM' to move a ship or '3,4' to fire");
        }
    }

    private void actionMove(int x, int y, char[] actions) {
        ShipImpl shipToMove = battleShipGame.getShip(x,y);
        if(shipToMove!=null) {
            for(int i=0;i<actions.length;i++){
                if(actions[i]=='M'){
                    try{
                        battleShipGame.moveShip(shipToMove);
                    } catch (SquareOccupiedException e) {
                        System.out.println(e.getMessage());
                    } catch (OutOfBoardBoundaryException e) {
                        System.out.println(e.getMessage());
                    }
                }
                else
                    battleShipGame.turnShip(shipToMove,getTurn(actions[i]));
            }
        } else {
            System.out.println("There is no ship at ("+x+","+y+")");
        }
    }

    private void actionFire(int x, int y) {
        battleShipGame.fire(x,y);
    }
    private Direction getDirection(char direction) {
        switch (direction){
            case 'N' : return Direction.North;
            case 'W' : return Direction.West;
            case 'E' : return Direction.East;
            case 'S' : return Direction.South;
        }
        return null;
    }

    private Turn getTurn(char turn) {
        switch (turn) {
            case 'L' : return Turn.Left;
            case 'R' : return Turn.Right;
        }
        return null;
    }
}
