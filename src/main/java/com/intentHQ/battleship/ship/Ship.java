package com.intentHQ.battleship.ship;

public interface Ship {

    /**
     * Move a ship
     */
    void move();

    /**
     * turn the facing of a ship
     * @param turn the direction, L means left R means right
     */
    void turn(Turn turn);

}
