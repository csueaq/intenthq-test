package com.intentHQ.battleship.ship.impl;

import com.intentHQ.battleship.ship.Direction;
import com.intentHQ.battleship.ship.Ship;
import com.intentHQ.battleship.ship.Turn;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


public class ShipImpl implements Ship {

    private int coordinateX;
    private int coordinateY;
    private int coordinateXBoundary = Integer.MAX_VALUE;
    private int coordinateYBoundary = Integer.MAX_VALUE;
    private Direction facing;
    private boolean isSunk;

    public ShipImpl(int x, int y, Direction facing){
        coordinateX = x;
        coordinateY = y;
        this.facing = facing;
        isSunk = false;
    }
    @Override
    public void move() {
        switch (facing) {
            case North : setCoordinateY(coordinateY + 1);break;
            case East  : setCoordinateX(coordinateX + 1);break;
            case South : setCoordinateY(coordinateY - 1);break;
            case West  : setCoordinateX(coordinateX - 1);break;
        }
    }

    public int getNextMovingCoordinateY() {
        int result = getCoordinateY();
        switch (facing) {
            case North : result = coordinateY + 1;break;
            case South : result = coordinateY - 1;break;
        }
        return result;
    }

    public int getNextMovingCoordinateX() {
        int result = getCoordinateX();
        switch (facing) {
            case East : result = coordinateX + 1;break;
            case West : result = coordinateX - 1;break;
        }
        return result;
    }

    @Override
    public void turn(Turn turn) {
        if (turn == Turn.Left)
        switch (facing) {
            case North : setFacing(Direction.West);break;
            case East  : setFacing(Direction.North);break;
            case South : setFacing(Direction.East);break;
            case West  : setFacing(Direction.South);break;
        } else if (turn == Turn.Right)
            switch (facing) {
                case North : setFacing(Direction.East);break;
                case East  : setFacing(Direction.South);break;
                case South : setFacing(Direction.West);break;
                case West  : setFacing(Direction.North);break;
            }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(coordinateX)
                .append(coordinateY)
                .append(coordinateXBoundary)
                .append(coordinateYBoundary)
                .append(facing)
                .append(isSunk)
                .toHashCode();
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof ShipImpl)) {
            return false;
        }
        ShipImpl rhs = (ShipImpl) obj;
        return new EqualsBuilder()
                .append(coordinateX, rhs.coordinateX)
                .append(coordinateY, rhs.coordinateY)
                .append(coordinateXBoundary, rhs.coordinateXBoundary)
                .append(coordinateYBoundary, rhs.coordinateYBoundary)
                .append(facing, rhs.facing)
                .append(isSunk, rhs.isSunk)
                .isEquals();
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        if(coordinateX >= 0 ) {
            if(coordinateX <= getCoordinateXBoundary()){
                this.coordinateX = coordinateX;
            }
        }
    }
    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        if(coordinateY >= 0 ) {
            if(coordinateY <= getCoordinateYBoundary()){
                this.coordinateY = coordinateY;
            }
        }
    }

    public void setFacing(Direction facing) {
        this.facing = facing;
    }

    public Direction getFacing() {
        return facing;
    }

    public int getCoordinateXBoundary() {
        return coordinateXBoundary;
    }

    public void setCoordinateXBoundary(int coordinateXBoundary) {
        this.coordinateXBoundary = coordinateXBoundary;
    }

    public int getCoordinateYBoundary() {
        return coordinateYBoundary;
    }

    public void setCoordinateYBoundary(int coordinateYBoundary) {
        this.coordinateYBoundary = coordinateYBoundary;
    }

    public boolean isSunk() {
        return isSunk;
    }

    public void setSunk(boolean sunk) {
        isSunk = sunk;
    }
}
