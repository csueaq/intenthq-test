package com.intentHQ.battleship.board;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Board {


    private SquareStatus[][] coordinate;
    private int coordinateXBoundary;
    private int coordinateYBoundary;

    /**
     * Create a board based on given size on X and Y
     * @param numberOfSquaresOnX the number Of Squares On X,
     * @param numberOfSquaresOnY the number Of Squares On Y
    * */
    public Board (int numberOfSquaresOnX, int numberOfSquaresOnY) {
        setCoordinate(new SquareStatus[numberOfSquaresOnX][numberOfSquaresOnY]);
        setCoordinateXBoundary(numberOfSquaresOnX - 1);
        setCoordinateYBoundary(numberOfSquaresOnY - 1);
    }

    /**
     * Initialise a board, set all the square to be empty
    * */
    public void init() {
        for(int x=0 ; x <= coordinateXBoundary ; x++ )
            for(int y=0 ; y <= coordinateYBoundary ; y++ )
                setEmptyAtCoordinate(x,y);
    }

    public void setOccupiedAtCoordinate(int coordinateX,int coordinateY) {
        coordinate[coordinateX][coordinateY] = SquareStatus.Occupied;
    }

    public void setShipSunkAtCoordinate(int coordinateX,int coordinateY) {
        coordinate[coordinateX][coordinateY] = SquareStatus.ShipSunk;
    }
    public void setEmptyAtCoordinate(int coordinateX,int coordinateY) {
        coordinate[coordinateX][coordinateY] = SquareStatus.Empty;
    }

    public SquareStatus getSquareStatus(int coordinateX,int coordinateY){
        return coordinate[coordinateX][coordinateY];
    }

    public void setCoordinate(SquareStatus[][] coordinate) {
        this.coordinate = coordinate;
    }

    public int getCoordinateXBoundary() {
        return coordinateXBoundary;
    }

    public void setCoordinateXBoundary(int coordinateXBoundary) {
        this.coordinateXBoundary = coordinateXBoundary;
    }

    public int getCoordinateYBoundary() {
        return coordinateYBoundary;
    }

    public void setCoordinateYBoundary(int coordinateYBoundary) {
        this.coordinateYBoundary = coordinateYBoundary;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(coordinate)
                .append(coordinateXBoundary)
                .append(coordinateYBoundary)
                .toHashCode();
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof Board)) {
            return false;
        }
        Board rhs = (Board) obj;
        return new EqualsBuilder()
                .append(coordinate, rhs.coordinate)
                .append(coordinateXBoundary, rhs.coordinateXBoundary)
                .append(coordinateYBoundary, rhs.coordinateYBoundary)
                .isEquals();
    }
}
